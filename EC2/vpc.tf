resource "aws_vpc" "nginx_vpc" {

  cidr_block = var.vpc_cidr
  tags = {
    "Name" = "nginx-vpc"
  }
}

resource "aws_internet_gateway" "nginx_igw" {
  vpc_id = aws_vpc.nginx_vpc.id

  tags = {
    "Name" = "vpc-igw"
  }
}

resource "aws_subnet" "nginx_public_subnet" {
  vpc_id                  = aws_vpc.nginx_vpc.id
  cidr_block              = var.public_subnet_cidr
  map_public_ip_on_launch = true
  availability_zone       = var.availability_zone
  tags = {
    "Name" = "nginx-public-subnet"
  }
}

resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.nginx_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.nginx_igw.id
  }
  tags = {
    "Name" = "nginx-public-rt"
  }
}

resource "aws_route_table_association" "public_rt_assoc" {
  subnet_id      = aws_subnet.nginx_public_subnet.id
  route_table_id = aws_route_table.public_rt.id
}


