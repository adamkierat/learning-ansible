output "web_instance_1_ip" {
  value = aws_instance.nginx_1.public_ip
}
output "web_instance_2_ip" {
  value = aws_instance.nginx_2.public_ip
}
