#!/bin/bash
sudo yum update -y
sudo amazon-linux-extras install nginx1 -y
sudo systemctl enable nginx
sudo systemctl start nginx
mkdir /tmp/test
touch /tmp/test/index.html
echo "<h1>Hello World</h1>" > /tmp/test/index.html