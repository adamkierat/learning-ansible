variable "aws_region" {
  default = "eu-west-1"
}
variable "availability_zone" {
  default = "eu-west-1a"
}
variable "instance_type" {
  default = "t2.micro"
}
variable "vpc_cidr" {
  default = "178.0.0.0/16"
}
variable "public_subnet_cidr" {
  default = "178.0.0.0/16"
}
