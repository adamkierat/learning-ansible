resource "aws_instance" "nginx_1" {
  ami             = "ami-0d71ea30463e0ff8d"
  instance_type   = var.instance_type
  key_name        = "wsl2_login"
  subnet_id       = aws_subnet.nginx_public_subnet.id
  security_groups = [aws_security_group.sg.id]
  user_data       = file("nginx_install.sh")

  tags = {
    Name = "nginx-instance-1"
  }
  volume_tags = {
    Name = "nginx-instance-1"
  }
}

resource "aws_instance" "nginx_2" {
  ami             = "ami-0d71ea30463e0ff8d"
  instance_type   = var.instance_type
  key_name        = "wsl2_login"
  subnet_id       = aws_subnet.nginx_public_subnet.id
  security_groups = [aws_security_group.sg.id]
  user_data       = file("nginx_install.sh")

  tags = {
    Name = "nginx-instance-2"
  }
  volume_tags = {
    Name = "nginx-instance-2"
  }
}
